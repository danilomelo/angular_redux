import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginCallback } from './pages/login/login.callback.component';
import { LoginComponent} from './pages/login/login.component';
import { ListComponent } from './pages/list/list.component';

const routes: Routes = [
  {path: '', component: LoginComponent },
  {path: 'callback', component: LoginCallback },
  {path: 'list', component: ListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
