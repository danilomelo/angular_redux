import { createAction, Action, props } from '@ngrx/store';
export const LOGIN__GET_ACCESS_TOKEN = '[LOGIN] Get access token';
export const LOGIN__NAVIGATE_TO_LIST = '[LOGIN] Navigate to List';
export const LOGIN__NAVIGATE_TO_LOGIN = '[LOGIN] Navigate to Login';
export const LOGIN__LOGIN_SUCCESS = '[LOGIN] Login Success';
export const LOGIN__GET_CODE = '[LOGIN] Login Get code';



export const loginSuccess = createAction(
	LOGIN__LOGIN_SUCCESS,
	props<{}>()
)

export const getCode = createAction(
	LOGIN__GET_CODE,
	props<{}>()
)

export const getAccessToken = createAction(
	LOGIN__GET_ACCESS_TOKEN,
	props<{}>()
)

export const navigateToList = createAction(
	LOGIN__NAVIGATE_TO_LIST
)

export const navigateToLogin = createAction(
	LOGIN__NAVIGATE_TO_LOGIN
)