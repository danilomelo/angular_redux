import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginCallback } from './login.callback.component';
import { ActivatedRoute, ParamMap, Params, convertToParamMap } from '@angular/router';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { AppState } from 'src/app/reducers/app.reducers';
import { Store, select } from '@ngrx/store';
import { ReplaySubject } from 'rxjs';
import { getAccessToken } from './actions/login.action';

// https://angular.io/guide/testing#routed-components
export class ActivatedRouteStub {
  // static setParamMap(params: Params) {
  //   throw new Error("Method not implemented.");
  // }
  // Use a ReplaySubject to share previous values with subscribers
  // and pump new values into the `paramMap` observable
  private subject = new ReplaySubject<ParamMap>();

  constructor(initialParams?: Params) {
    this.setParamMap(initialParams);
  }

  /** The mock paramMap observable */
  readonly paramMap = this.subject.asObservable();

  /** Set the paramMap observables's next value */
  setParamMap(params?: Params) {
    this.subject.next(convertToParamMap(params));
  };
}

describe('LoginCallback', () => {
  let component: LoginCallback;
  let fixture: ComponentFixture<LoginCallback>;
  let code = {
    code: 'asdf1234'
  };
  let store = {
    pipe: jasmine.createSpy('pipe').and.returnValue(code),
    dispatch: jasmine.createSpy('dispatch')
  };
  let params: Params = {'code': 'asdf1234'};
  let activatedRoute = new ActivatedRouteStub(params);

	const initialState = {
    app:{
      userdata: {},
      accessToken: '',
      repos: []
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
			declarations: [ LoginCallback ],
			providers:[
				{provide: ActivatedRoute, useValue: activatedRoute},
				{provide: Store, useValue: store}
			]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginCallback);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get code param and dispatch a action ', () => {
    expect(store.dispatch).toHaveBeenCalledWith(getAccessToken(code));
  });
});

