import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { credentials } from '../../github.credentials';

@Injectable()
export class httpServiceLogin{
	constructor(private http: HttpClient){}

	getAccessToken(param){
		return this.http.get('https://cors-anywhere.herokuapp.com/https://github.com/login/oauth/access_token', {
			headers:{
				Accept: 'application/json'
			},
			params: {
				client_id: credentials.clientID,
				client_secret: credentials.clientSecret,
				code: param.code,
			}
		})
	}

}