import { Component, OnInit } from '@angular/core';
import { credentials } from '../../github.credentials';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit() { }

  credentials = credentials;
}
