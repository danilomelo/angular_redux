import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { getAccessToken } from './actions/login.action';
import { Store } from '@ngrx/store';

@Component({
  selector: 'login-callback',
  templateUrl: './login.callback.component.html',
})

export class LoginCallback implements OnInit {

	constructor(private route: ActivatedRoute, private store: Store<{code: string}>) { }

	ngOnInit() {
		let code = this.route.snapshot.queryParamMap.get('code');
		this.store.dispatch(getAccessToken( {code} ));	
	}
}