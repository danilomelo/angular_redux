import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState, selectListRepo } from 'src/app/reducers/app.reducers';
import { getListRepos } from './actions/list.actions';
import { LOGIN__ACCESS_TOKEN_NAME } from '../login/login.constants';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  repos: Observable<Object[]>;

  constructor(private store:Store<AppState>) { }
  
  ngOnInit() {
    this.store.dispatch(getListRepos({token: localStorage.getItem(LOGIN__ACCESS_TOKEN_NAME)}))
    this.repos = this.store.pipe(select(selectListRepo));
  }
}
