import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, MemoizedSelector, select } from '@ngrx/store';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { ListComponent } from './list.component';
import { Router }       from '@angular/router';
import { navigateToLogin } from '../login/actions/login.action';
import { AppState, selectAccesstoken, setListRepos, selectListRepo } from 'src/app/reducers/app.reducers';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let store: MockStore<AppState>;
  let fk_selectAccesstoken: MemoizedSelector<AppState, Object[]>;

  const initialState = {
    app:{
      userdata: {},
      accessToken: '',
      repos: []
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListComponent ],
      providers:[
        provideMockStore({initialState})
      ]
    })
    .compileComponents();

    store = TestBed.get(Store);
    fk_selectAccesstoken = store.overrideSelector(selectListRepo, []); 
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('shoud no have items in repos array', ()=>{
    fixture.detectChanges();
    component.repos.subscribe((arr)=>{
      expect(arr.length).toEqual(0);
    });
  });

  it('shoud no have items in repos array', ()=>{
    fixture.detectChanges();
    fk_selectAccesstoken.setResult([{name: 'New repo'}])
    component.repos.subscribe((arr)=>{
      expect(arr.length).toEqual(1);
    });
  });

});
