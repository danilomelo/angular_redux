import { createAction, props } from '@ngrx/store';

export const LIST__GET_LIST_REPOS = '[LIST] Get list repos';
export const LIST__LIST_REPOS = '[LIST] List repos';

export const getListRepos = createAction(
	LIST__GET_LIST_REPOS, 
	props<{token:string}>()
);

export const listRepos = createAction(
	LIST__LIST_REPOS,
	props<{repos: Object[]}>()
);