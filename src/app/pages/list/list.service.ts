import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';

@Injectable()
export class httpServiceList{
	constructor(private http: HttpClient){}

	getListRepos(access_token){
		return this.http.get('https://api.github.com/user/repos', {
			headers:{
				Accept: 'application/json'
			},
			params: {
				access_token
			}
		});
	}
}