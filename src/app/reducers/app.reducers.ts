import { createReducer, on, Action, createSelector } from '@ngrx/store';
import { loginSuccess } from '../pages/login/actions/login.action';
import { listRepos } from '../pages/list/actions/list.actions';
import { LOGIN__ACCESS_TOKEN_NAME } from '../pages/login/login.constants';

export interface AppState {
	userdata: object,
	accessToken: string,
	repos: Array<{}>
}

export const initialState: AppState = {
	userdata: {},
	accessToken: '',
	repos: []
};

export const setAccessToken = (state, action):AppState => {
	localStorage.setItem(LOGIN__ACCESS_TOKEN_NAME, action.access_token);

	return Object.assign({}, state, {
		...state,
		accessToken: action.access_token
	});
}

export const setListRepos = (state, action):AppState => {
	return Object.assign({}, state, {
		...state,
		repos: action.repos
	});
}

export const selectAppState = (state) => state.app;

export const selectListRepo = createSelector(
	selectAppState,
	(state) => state.repos
)

export const selectAccesstoken = createSelector(
	selectAppState,
	(state) => state.accessToken || localStorage.getItem(LOGIN__ACCESS_TOKEN_NAME)
)

export const AppReducer = createReducer(
	initialState, 
	on(loginSuccess, setAccessToken),
	on(listRepos, setListRepos)
)

export function reducers(state: AppState, action: Action){
	return AppReducer(state, action);
}