import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap, mergeMap, tap, catchError } from 'rxjs/operators';
import { httpServiceList } from './pages/list/list.service';
import { getAccessToken, loginSuccess, navigateToList, navigateToLogin } from './pages/login/actions/login.action';
import { httpServiceLogin } from './pages/login/login.service';
import { listRepos, getListRepos } from './pages/list/actions/list.actions';
import { Router } from '@angular/router';
import { of } from 'rxjs';
 
@Injectable()
export class appEffects{
	constructor(private actions$: Actions, private httpList: httpServiceList, private httpLogin: httpServiceLogin, private router:Router){}

	@Effect()
	getAccessToken$ = this.actions$.pipe(
		ofType(getAccessToken),
		switchMap((action) => {
				return this.httpLogin.getAccessToken(action).pipe(
					mergeMap(
						(res) => [ 
							loginSuccess(res),
							navigateToList()
						]
					)
				)
			}
		)
	)

	@Effect({ dispatch: false })
	navigateToList$ = this.actions$.pipe(
			ofType(navigateToList),
			tap(() => this.router.navigate(['list']))
	);
	
	@Effect({ dispatch: false })
	navigateToLogin$ = this.actions$.pipe(
			ofType(navigateToLogin),
			tap(() => this.router.navigate(['']))
	);

	@Effect()
	getListRepos$ = this.actions$.pipe(
		ofType(getListRepos),
		switchMap((action) => this.httpList.getListRepos(action.token).pipe(
			map((data:Object[])=>listRepos({repos: data})),
			catchError( () => of( navigateToLogin() ) )
		))
	)
}