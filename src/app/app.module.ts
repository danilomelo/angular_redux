import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { reducers } from './reducers/app.reducers';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { LoginCallback } from './pages/login/login.callback.component';
import { ListComponent } from './pages/list/list.component';
import { appEffects } from './effects';
import { httpServiceList } from './pages/list/list.service';
import { httpServiceLogin } from './pages/login/login.service';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginCallback,
    ListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({app: reducers}),
    EffectsModule.forRoot([appEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, 
      logOnly: environment.production,
    }),
  ],
  providers: [
    HttpClient,
    appEffects, 
    httpServiceList, 
    httpServiceLogin, 
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
