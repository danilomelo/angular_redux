import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { LOGIN__ACCESS_TOKEN_NAME } from './pages/login/login.constants';
import { Router } from '@angular/router';
import { provideMockStore } from '@ngrx/store/testing';

describe('AppComponent', () => {
  const routerSpy = jasmine.createSpyObj('router', ['navigate']);
  const initialState = {
    app:{
      userdata: {},
      accessToken: '',
      repos: []
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers:[
        { provide: Router, useValue: routerSpy },
        provideMockStore({initialState})
      ]
    }).compileComponents();
  }));

  it('should render the navbar with a text: Danilo Rodrigues - Teste para o Banco Pan', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.navbar .navbar-brand').textContent).toContain('Danilo Rodrigues - Teste para o Banco Pan');
  });

  it('should be redirected to login view because accessToken is undefined in localStorage', ()=>{
    localStorage.setItem(LOGIN__ACCESS_TOKEN_NAME, undefined);
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['list']);
  });

  it('should be redirected to list view because accessToken is defined in localStorage', ()=>{
    localStorage.setItem(LOGIN__ACCESS_TOKEN_NAME, 'asdf');
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['list']);
  });
 
});
