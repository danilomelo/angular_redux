import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LOGIN__ACCESS_TOKEN_NAME } from './pages/login/login.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor(private router: Router){ }
  
  ngOnInit(){
    if(!localStorage.getItem(LOGIN__ACCESS_TOKEN_NAME)){
      return this.router.navigate([''])
    }else{
      return this.router.navigate(['list']);
    }

  }
}
